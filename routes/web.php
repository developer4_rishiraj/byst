<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */
/* Route::get('/home', function () {
    return view('welcome');
}); */
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/get_state_bycid', 'HomeController@get_state_bycid');
Route::get('/get_citiy_bycid', 'HomeController@get_citiy_bycid');
Route::get('/counter', 'HomeController@counter')->name('counter');
Route::get('/bystexpo', 'HomeController@bystexpo')->name('bystexpo');
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

