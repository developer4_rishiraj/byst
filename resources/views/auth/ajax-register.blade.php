<form id="registerfrom" method="POST" autocomplete="off" action="{{ route('register') }}">
	@csrf
	<label class="form-group has-float-label mb-4">
		<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="off" autofocus>
		 @error('name')
			<span class="invalid-feedback register-error-msg" role="alert">
				<strong class="alert-danger">{{ $message }}</strong>
			</span>
		@enderror
		<span>{{ __('Name') }}</span> 
	</label>
	
	<label class="form-group has-float-label mb-4">
		<input id="email" type="email" autocomplete="off" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required>
		 @error('email')
			<span class="invalid-feedback register-error-msg" role="alert">
				<strong class="alert-danger">{{ $message }}</strong>
			</span>
		@enderror
		<span>{{ __('E-mail') }}</span>
	</label>

	<label class="form-group has-float-label mb-3">
		<input id="password" type="password" autocomplete="off" class="regpassword form-control @error('password') is-invalid @enderror" name="password" required>
		@error('password')
			<span class="invalid-feedback register-error-msg" role="alert">
				<strong class="alert-danger">{{ $message }}</strong>
			</span>
		@enderror
		<span>{{ __('Password (8 or more Characters)') }}</span> 
		
	</label>
	<span class="pass-error" style="color:red; font-size:14px;"></span><br>
	<label class="form-group has-float-label mb-4">
		<input id="organisation" type="text" class="form-control @error('organisation') is-invalid @enderror" name="organisation" value="{{ old('organisation') }}" required autocomplete="off" autofocus>
		 @error('organisation')
			<span class="invalid-feedback register-error-msg" role="alert">
				<strong class="alert-danger">{{ $message }}</strong>
			</span>
		@enderror
		<span>{{ __('Organisation') }}</span> 
	</label>
	
	<label class="form-group has-float-label mb-4">
		<input id="designation" type="text" class="form-control @error('designation') is-invalid @enderror" name="designation" value="{{ old('designation') }}" required autocomplete="off" autofocus>
		 @error('designation')
			<span class="invalid-feedback register-error-msg" role="alert">
				<strong class="alert-danger">{{ $message }}</strong>
			</span>
		@enderror
		<span>{{ __('Designation') }}</span> 
	</label>
	
	<label class="form-group has-float-label mb-4">
		<input id="mobile_no" type="text" class="form-control @error('mobile_no') is-invalid @enderror" name="mobile_no" value="{{ old('mobile_no') }}" required autocomplete="off" autofocus>
		 @error('mobile_no')
			<span class="invalid-feedback register-error-msg" role="alert">
				<strong class="alert-danger">{{ $message }}</strong>
			</span>
		@enderror
		<span>{{ __('Mobile') }}</span> 
	</label>
	
	<label class="form-group has-float-label mb-4">
		<select class="form-control select2-single country_id" name="country_id" id="country_id" data-width="100%">
			<option value="">Select Country</option>
			@foreach($countries as $obj)
				<option value="{{ $obj->id }}">{{ $obj->name }}</option>
			@endForeach
		  </select>
		 @error('country_no')
			<span class="invalid-feedback register-error-msg" role="alert">
				<strong class="alert-danger">{{ $message }}</strong>
			</span>
		@enderror
		<span>{{ __('Country') }}</span> 
	</label>
	
	<label class="form-group has-float-label mb-4">
		<select class="form-control select2-single state_id" name="state_id" id="state_id" data-width="100%">
			<option value="">Select State</option>
		  </select>
		 @error('state_no')
			<span class="invalid-feedback register-error-msg" role="alert">
				<strong class="alert-danger">{{ $message }}</strong>
			</span>
		@enderror
		<span>{{ __('State') }}</span> 
	</label>
	
	<label class="form-group has-float-label mb-4">
		<select class="form-control select2-single city_id" name="city_id" id="city_id" data-width="100%">
			<option value="">Select City</option>
		  </select>
		 @error('city_no')
			<span class="invalid-feedback register-error-msg" role="alert">
				<strong class="alert-danger">{{ $message }}</strong>
			</span>
		@enderror
		<span>{{ __('City') }}</span> 
	</label>
	
	<p class="form-sidep regpasswordvalid"></p>
	
	<div class="d-flex justify-content-end align-items-center mb-2"> 
		<button class="rfloat btn  btn-theme2 scroll-to animated flipInY visible active" type="submit">Register <img class="register-loader" style="display:none" src="{{ asset('loader.gif') }}" /></button>
		<strong class="alert-success successreg" style="display:none" >Wellcome, you are register successfully</strong>
	</div>
	
</form>