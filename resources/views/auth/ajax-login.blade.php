<h6 class="mb-4">Login</h6>
<form method="POST" action="{{ route('login') }}" id="loginform">
	@csrf

  <label class="form-group has-float-label mb-4">
		<input id="email_log" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  required autocomplete="email">

		@error('email')
			<span class="invalid-feedback login-error-message text-danger" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
		<span>E-mail</span>
	</label>

	<label class="form-group has-float-label mb-4">
		<input id="password_log" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>
		@error('password')
			<span class="invalid-feedback login-error-message text-danger" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
		<span>Password</span>
	</label>
	
	<div class="d-flex justify-content-between align-items-center"> 
		@if (Route::has('password.request'))
			<a class="forget-link modal-forget-link" href="#" data-href="{{ route('password.request') }}">
				{{ __('Forgot Your Password?') }}
			</a>
		@endif
		<button class="login__btn login-button-submit" type="submit">
			Login
			<img class="login-loader" style="display:none" src="{{ asset('loader.gif') }}" /></button>
	</div>
</form>