<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Virtual Event</title>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->


<link rel="stylesheet" href="streem/bootstrap.min.css">
<link rel="stylesheet" href="streem/font-awesome.min.css">
<style>
body{ margin:0; padding:0;}
.header_main{ width:100%; height:auto; display:block;}
.header{ height:42px; width:1366px; padding:3px 0 0 10px; background:#2873f0; margin:0 auto;}
.body-bg{ background:#000000; background-size: cover;}
.main-bg{ position:relative; background:url(streem/img/main-bg2.jpg) no-repeat; width:1366px; height:724px; margin:0 auto;}
.screen{
	position:absolute;
	width:645px;
	height:369px;
	left:359px;
	top:120px;
}
.feedback{
	position:absolute;
	width:131px;
	height:44px;
	left: 620px;
	top: 670px;
}
.accordion_main{
	position:absolute;
	width:280px;
	height:40px;
	left: 1020px;
	top: 147px;
}

.login{ text-align:right; color:#fff; float:right; margin:8px 20px 0 0;}
.logout{ text-align:right; color:#fff;float:right; margin:8px 20px 0 0;}
.login:hover{ color:#fff;}
.logout:hover{ color:#fff;}


</style>
<style>
@charset "UTF-8";
body {
	background: #e0e0e0;
	padding:0;
}
.content {
	background: #fff;
	border-radius: 3px;
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.075), 0 2px 4px rgba(0, 0, 0, 0.0375);
	padding: 30px;
}
.panel-group {
	margin-bottom: 0;
}
.panel-group .panel {
	border-radius: 0;
	box-shadow: none;
}
.panel-group .panel .panel-heading {
	padding: 0;
}
.panel-group .panel .panel-heading h4 a {
	background: #237125;
	display: block;
	font-size: 14px;
	font-weight: 600;
	padding: 15px;
	text-decoration: none;
	transition: 0.15s all ease-in-out;
}
.panel-group .panel .panel-heading h4 a:hover, .panel-group .panel .panel-heading h4 a:not(.collapsed) {
 background:none;
 transition: 0.15s all ease-in-out;
}
.panel-group .panel .panel-heading h4 a:not(.collapsed) i:before {
 content: "";
}
.panel-group .panel .panel-heading h4 a i {
	color: #ccc;
}
.panel-group .panel .panel-body {
	/*padding-top: 0;*/
}
.panel-group .panel .panel-heading + .panel-collapse > .list-group, .panel-group .panel .panel-heading + .panel-collapse > .panel-body {
	border-top: none;
}
.panel-group .panel + .panel {
	border-top: none;
	margin-top: 0;
}



/*-------------popup------------*/

.box {
  width: 40%;
  margin: 0 auto;
  background: rgba(255,255,255,0.2);
  padding: 35px;
  border: 2px solid #fff;
  border-radius: 20px/50px;
  background-clip: padding-box;
  text-align: center;
}

.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
}
.overlay:target {
  visibility: visible;
  opacity: 1;
}

.popup {
  margin: 70px auto;
  padding: 20px;
  background: #fff;
  border-radius: 5px;
  width: 40%;
  position: relative;
  transition: all 5s ease-in-out;
}

.popup h2 {
  margin-top: 0;
  color: #333;
  font-family: Tahoma, Arial, sans-serif;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
  color: #06D85F;
}
.popup .content {
  max-height: 30%;
  overflow: auto;
}

@media screen and (max-width: 700px){
  .box{
    width: 100%;
  }
  .popup{
    width: 100%;
  }
  .popup {
  width: 80%;
}
}
.btn {
  background-color: DodgerBlue;
  border: none;
  color: white;
  padding: 6px 20px;
  cursor: pointer;
  font-size: 16px; 
  margin:0 auto;
  float: right;
}
</style>
</head>

<body class="body-bg">
<div class="header_main">
<div class="header">
<img src="streem/img/logo.png" width="190" height="37" />

				
<a href="{{ route('logout') }}" data-url="{{ route('logout') }}" onclick="event.preventDefault();
									 document.getElementById('logout-form').submit();" class="rfloat btn btn-theme scroll-to animated flipInY visible" data-animation="flipInY" data-animation-delay="200">Logout <i class="fa fa-arrow-circle-right"></i></a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	@csrf
</form>

<a href="#" class="login">{{Auth::user()->name}}</a>
</div>
</div>
<div class="main-bg">
<!--<a href="#" class="fb"></a>
<a href="#" class="insta"></a>
<a href="#" class="tw"></a>-->
<div class="screen">
<iframe scrolling src="http://account13.livebox.co.in/livebox/player/?chnl=enseur" width="645" height="369" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen allow="autoplay" ></iframe>

</div>
<a class="feedback button" href="#popup1"><img src="streem/img/feedback.png" width="116" height="40" /></a>
<!--<a href="#" class="link"><img src="link.png" width="134" height="30" /></a>-->


  <div class="accordion_main">
  
  
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      
      <div class="panel panel-default">
        <div class="panel-heading" id="headingTwo" role="tab">
          <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="https://cdpn.io/johnfinkdesign/fullpage/xEVqvP#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">AGENDA<i class="pull-right fa fa-plus"></i></a></h4>
        </div>
        <div class="panel-collapse collapse" id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
          <div class="panel-body">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial;font-size:11px;">
  <tr>
    <td colspan="3" style="border-bottom:solid 2px #dddddd; padding:5px;" align="center" bgcolor="#f8f8f8"><strong>Thursday, 23rd May 2021</strong></td>
  </tr>
  <tr>
    <td align="left" valign="top" style=" padding:5px;">7:00 am <br /> 9:00 pm</td>
    <td align="left" valign="top" style=" padding:5px;"><strong>Opening Speach</strong>
    <p style=" margin:2px 0 0px 0; font-size:12px;">• Simply dummy text</p>
    <p style=" margin:0 0 0px 0; font-size:12px;">• Dummy text of</p>
    </td>
    <td align="left" valign="top" style=" padding:5px;">Room2</td>
  </tr>
  <tr>
    <td colspan="3" style="border-bottom:solid 2px #dddddd; padding:5px;" align="center" bgcolor="#f8f8f8"><strong>Thursday, 23rd May 2021</strong></td>
  </tr>
  <tr>
    <td align="left" valign="top" style=" padding:5px;">7:00 am <br /> 9:00 pm</td>
    <td align="left" valign="top" style=" padding:5px;"><strong>Opening Speach</strong>
    <p style=" margin:2px 0 0px 0; font-size:12px;">• Simply dummy text the</p>
    </td>
    <td align="left" valign="top" style=" padding:5px;">Room3</td>
  </tr>
  <tr>
    <td colspan="3" style="border-bottom:solid 2px #dddddd; padding:5px;" align="center" bgcolor="#f8f8f8"><strong>Thursday, 23rd May 2021</strong></td>
  </tr>
  <tr>
    <td align="left" valign="top" style=" padding:5px;">7:00 am <br /> 9:00 pm</td>
    <td align="left" valign="top" style=" padding:5px;"><strong>Opening Speach</strong>
    <p style=" margin:2px 0 0px 0; font-size:12px;">• Simply dummy text</p>
    <p style=" margin:0 0 0px 0; font-size:12px;">• Dummy text of</p>
    </td>
    <td align="left" valign="top" style=" padding:5px;">Room2</td>
  </tr>
  <tr>
    <td colspan="3" style="border-bottom:solid 2px #dddddd; padding:5px;" align="center" bgcolor="#f8f8f8"><strong>Thursday, 23rd May 2021</strong></td>
  </tr>
  <tr>
    <td align="left" valign="top" style=" padding:5px;">7:00 am <br /> 9:00 pm</td>
    <td align="left" valign="top" style=" padding:5px;"><strong>Opening Speach</strong>
    <p style=" margin:2px 0 0px 0; font-size:12px;">• Simply dummy text the</p>
    </td>
    <td align="left" valign="top" style=" padding:5px;">Room3</td>
  </tr>
</table>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>
<div id="popup1" class="overlay">
	<div class="popup">
		<h4>Feedback</h4>
		<a class="close" href="#">&times;</a>
		<div>
        <form>
		<textarea name="" cols="" rows="4" style="width:100%; border:solid 1px #CCC;"></textarea>
        <div style="display:block; text-align:center; margin-top:14px;"><button class="btn">Submit</button></div>
        </form>	
		</div>
	</div>
</div>
<script src='streem/jquery.min.js'></script>
<script src='streem/bootstrap.min.js'></script>
</body>
</html>




