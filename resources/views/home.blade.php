<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Optimizemeet</title>

<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="shortcut icon" href="">

<!-- CSS Global -->
<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
<link href="assets/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
<link href="assets/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
<link href="assets/plugins/prettyphoto/css/prettyPhoto.css" rel="stylesheet">
<link href="assets/plugins/animate/animate.min.css" rel="stylesheet">
<link href="assets/plugins/countdown/jquery.countdown.css" rel="stylesheet">
<link href="assets/css/theme.css" rel="stylesheet">
<link href="assets/css/custom.css" rel="stylesheet">

<!--[if lt IE 9]>
    <script src="assets/plugins/iesupport/html5shiv.js"></script>
    <script src="assets/plugins/iesupport/respond.min.js"></script>
    <![endif]-->
</head>
<body id="home" class="wide body-light">

<!-- Preloader -->
<div id="preloader">
  <div id="status">
    <div class="spinner"></div>
  </div>
</div>

<!-- Wrap all content -->
<div class="wrapper"> 
  
  <!-- HEADER -->
  <header class="header fixed">
    <div class="container">
      <div class="header-wrapper clearfix"> 
        
        <!-- Logo -->
        <div class="logo"> <a href="#home" class="logo-web"></a> </div>
        <!-- /Logo --> 
        
        <!-- Navigation -->
        <div id="mobile-menu"></div>
        <nav class="navigation closed clearfix"> <a href="#" class="menu-toggle btn"><i class="fa fa-bars"></i></a>
          <ul class="sf-menu nav">
            <li class="active"><a href="#home">Home</a></li>
            <li><a href="#about">Features</a></li>
            <li><a href="#schedule">Pricing</a></li>
            <li><a href="#sponsors">FAQs</a></li>
            <li><a href="#speakers">Contact Us</a></li>
            <li><a href="#price">Sign In</a></li>
            <li><a href="#location">Signup, It's Free</a></li>
          </ul>
        </nav>
        <!-- /Navigation --> 
        
      </div>
    </div>
  </header>
  <!-- /HEADER --> 
  
  <!-- Content area -->
  <div class="content-area">
    <div id="main"> 
      <!-- SLIDER -->
      <div class="home-banner"><img src="assets/img/banner.png"></div> 
      <!-- /SLIDER --> 
    </div>
    
    <!-- PAGE ABOUT -->
    <section class="page-section" id="about">
      <div class="container">
        <div class="row border-bot">
        <div class="col-lg-6">
        <div style="display:block;">
        <h1 class="bottom-margin20 backcolor section-title lfloat">Global Skill Summit 2019 
        <a href="#" class="icon-bor"><i class="clapping-icon profile-clapping-icon"></i></a>
        <a href="#" class="icon-bor"><i class="fa fa-share-alt profile-clapping-icon"></i></a></h1>
        </div>
        <h3 class="bottom-margin10 backcolor section-title"><small><i class="fa fa-calendar"></i> 01-23 Jan 2020</small></h3>
        <h3 class="bottom-margin10 backcolor section-title"><small><i class="fa fa-map-marker"></i> New Delhi</small></h3>
        <div class="cl"></div>
        </div>
        <div class="col-lg-6">
        <div class="bottom-margin20">
		@auth
			<div class="right-side-box">
				
				<a href="{{ route('logout') }}" data-url="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="rfloat btn btn-theme scroll-to animated flipInY visible" data-animation="flipInY" data-animation-delay="200">Logout <i class="fa fa-arrow-circle-right"></i></a>
            
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
			</div>
        <!--<a href="{{ url('/index') }}">Home</a>-->
        @else
        @if (Route::has('register'))
        <div class="right-side-box"><a href="#register" data-toggle="modal" data-target=".register" class="rfloat btn btn-theme scroll-to animated flipInY visible" data-animation="flipInY" data-animation-delay="200">Register <i class="fa fa-arrow-circle-right"></i></a> 
		
         @endif
            @if (Route::has('login'))
            <a href="#login" data-toggle="modal" data-target=".login" class="rfloat btn btn-theme scroll-to animated flipInY visible" data-animation="flipInY" data-animation-delay="200">Sign in <i class="fa fa-arrow-circle-right"></i></a>
            @endif
        </div>
        @endif
		
		<div class="cl"></div>
        </div>
        <div class="d-block " style="float: right;"> 
		<a href="#" data-toggle="modal" data-target=".login"><img src="assets/img/preview/avatar-v2-3.jpg" alt="" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall"></a>        
        <a href="#"><button type="button" class="fc-today-button rounded-circle btn btn-primary loadcount disabled" disabled="">0</button></a> </div>
        <div class="cl"></div>
        </div>
        <div class="cl"></div>
        </div>
        <!---tab--->
        <div class="row">
        <!-- Schedule -->
        <div class="schedule-wrapper clear" data-animation="fadeIn" data-animation-delay="200">
          <div class="schedule-tabs lv1 bottom-margin20">
            <ul id="tabs-lv1"  class="nav nav-justified">
              <li class="active"><a href="#tab-first" data-toggle="tab"><strong>About</strong></a></li>
              <li><a href="#tab-second" data-toggle="tab"><strong>Speakers</strong></a></li>
              <li><a href="#tab-third" data-toggle="tab"><strong>Schedule</strong></a></li>
              <li><a href="#tab-four" data-toggle="tab"><strong>Sponsors</strong></a></li>
              <li><a href="#tab-last" data-toggle="tab"><strong>Contact</strong></a></li>
            </ul>
          </div>
          <div class="tab-content lv1"> 
            <!-- tab1 -->
            <div id="tab-first" class="tab-pane fade in active">
            <div class="row">
          <div class="col-lg-7">
          <a class="twitter-timeline" data-height="830" href="https://twitter.com/REInvestIndia?ref_src=twsrc%5Etfw">Tweets by REInvestIndia</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>  
            
          </div>
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-5 pull-left"> 
           <h2 class="backcolor"> Location </h2>
		   <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3502.4640501296753!2d77.243542!3d28.615851!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1db02b6d0ba0702a!2sPragati%20Maidan!5e0!3m2!1sen!2sin!4v1582177719872!5m2!1sen!2sin" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
           
           <div class="p10">
           <h3 class="backcolor"> Organizer </h3>
           <div class="media comment">
                    <a href="#" class="pull-left comment-avatar">
                        <img alt="" src="assets/img/preview/avatar-v2-3.jpg" class="media-object img-circle">
                    </a>
                    <div class="media-body">
                        <p class="comment-meta"><span class="comment-author"><a href="#">Rishiraj Media</a></span></p>
                        <p class="comment-text bottom-margin10">Entertainment . Retail</p>
                        <p class="comment-reply"><a href="#" class="sm-btn">Follow</a> <a href="#" class="sm-btn">Follow</a></p>
                    </div>
                </div>
           </div> 
           <!-- /Thumbnails --> 
            
          </div>
        </div>
            </div>
            <!-- tab2 -->
            <div id="tab-second" class="tab-pane fade">
            <h1 class="section-title"> <span data-animation="fadeInRight" data-animation-delay="100" class="title-inner"><small>Speakers</small></span> </h1>
             <div class="partners-carousel" data-animation="fadeInUp" data-animation-delay="300" data-items-xs="2" data-items-sm="3" data-items-lg="4" data-items-xl="5">
             <div class="owl-carousel">
            <div class="thumbnail no-border no-padding text-center">
              <div class="tabimg">
              <img src="assets/img/speaker/speaker.png">
              </div>
              <div class="caption before-media">
                <h3 class="caption-title">Speaker name here</h3>
                <p class="caption-category">Co Founder</p>
              </div>
              <div class="caption">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed vel velit</p>
              </div>
            </div>
            <div class="thumbnail no-border no-padding text-center">
              <div class="tabimg">
              <img src="assets/img/speaker/speaker.png">
              </div>
              <div class="caption before-media">
                <h3 class="caption-title">Speaker name here</h3>
                <p class="caption-category">Co Founder</p>
              </div>
              <div class="caption">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed vel velit</p>
              </div>
            </div>
            <div class="thumbnail no-border no-padding text-center">
              <div class="tabimg">
              <img src="assets/img/speaker/speaker.png">
              </div>
              <div class="caption before-media">
                <h3 class="caption-title">Speaker name here</h3>
                <p class="caption-category">Co Founder</p>
              </div>
              <div class="caption">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed vel velit</p>
              </div>
            </div>
            <div class="thumbnail no-border no-padding text-center">
              <div class="tabimg">
              <img src="assets/img/speaker/speaker.png">
              </div>
              <div class="caption before-media">
                <h3 class="caption-title">Speaker name here</h3>
                <p class="caption-category">Co Founder</p>
              </div>
              <div class="caption">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed vel velit</p>
              </div>
            </div>
            <div class="thumbnail no-border no-padding text-center">
              <div class="tabimg">
              <img src="assets/img/speaker/speaker.png">
              </div>
              <div class="caption before-media">
                <h3 class="caption-title">Speaker name here</h3>
                <p class="caption-category">Co Founder</p>
              </div>
              <div class="caption">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed vel velit</p>
              </div>
            </div>
            <div class="thumbnail no-border no-padding text-center">
              <div class="tabimg">
              <img src="assets/img/speaker/speaker.png">
              </div>
              <div class="caption before-media">
                <h3 class="caption-title">Speaker name here</h3>
                <p class="caption-category">Co Founder</p>
              </div>
              <div class="caption">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed vel velit</p>
              </div>
            </div>
             </div>
             </div>
            </div>
            <!-- tab3 -->
            <div id="tab-third" class="tab-pane fade">
            <h1 class="section-title"> <span data-animation="fadeInRight" data-animation-delay="100" class="title-inner"><small>Schedule</small></span> </h1>
            <div class="row">
            <div class="col-md-8">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            Monday - Mon,06 Jul,2020 - ● Online
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body accordion-border">
                        <h3 class="top-margin0 backcolor">1830 - 1930</h3>
                        <p class="bottom-margin0 backcolor">Hall 1 <span class="bullet-color">●</span> <b>Inaugural Ceremony</b></p>
                        <hr class="hr-accordion">
						<h3 class="top-margin0 backcolor">1830 - 1930</h3>
						<p class="bottom-margin0 backcolor">Hall 2A <span class="bullet-color">●</span> <b>Inaugural Ceremony</b></p>
                        </div>
                    </div>
                </div>
                <!--<div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Section 2
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros. </p>
                        </div>
                    </div>
                </div>-->
                <!--<div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Section 3
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros. </p>
                        </div>
                    </div>
                </div>-->
            </div>
            </div>
            </div>
            
            
            
            
            
            
            </div>
            <!-- tab4 -->
            <div id="tab-four" class="tab-pane fade">
            <h1 class="section-title"> <span data-animation="fadeInRight" data-animation-delay="100" class="title-inner"><small>Sponsors</small></span> </h1>
             <div class="partners-carousel" data-animation="fadeInUp" data-animation-delay="300" data-items-xs="2" data-items-sm="3" data-items-lg="4" data-items-xl="5">
             <div class="owl-carousel">
            <div><a href="#"><img src="assets/img/partner/light/partner-1.png" alt=""/></a></div>
            <div><a href="#"><img src="assets/img/partner/light/partner-2.png" alt=""/></a></div>
            <div><a href="#"><img src="assets/img/partner/light/partner-3.png" alt=""/></a></div>
            <div><a href="#"><img src="assets/img/partner/light/partner-4.png" alt=""/></a></div>
            <div><a href="#"><img src="assets/img/partner/light/partner-5.png" alt=""/></a></div>
            <div><a href="#"><img src="assets/img/partner/light/partner-6.png" alt=""/></a></div>
            <div><a href="#"><img src="assets/img/partner/light/partner-1.png" alt=""/></a></div>
            <div><a href="#"><img src="assets/img/partner/light/partner-2.png" alt=""/></a></div>
            <div><a href="#"><img src="assets/img/partner/light/partner-3.png" alt=""/></a></div>
            <div><a href="#"><img src="assets/img/partner/light/partner-4.png" alt=""/></a></div>
            <div><a href="#"><img src="assets/img/partner/light/partner-5.png" alt=""/></a></div>
            <div><a href="#"><img src="assets/img/partner/light/partner-6.png" alt=""/></a></div>
            </div>
             </div>
            </div>
            
          </div>
        </div>
        <!-- /Schedule --> 
        
        </div>
        <!---tab end--->
        
      </div>
    </section>
    
  </div>
  <!-- /Content area --> 
  
  <!-- FOOTER -->
  <footer class="footer">
    <div class="footer-meta">
      <div class="container text-center">
      <span class="copyright" data-animation="fadeInUp" data-animation-delay="100">Copyrights © 2019 All Rights Reserved by optimizemeet</span> </div>
    </div>
  </footer>
  <!-- /FOOTER -->
  
  <!--<div class="to-top"><i class="fa fa-angle-up"></i></div>-->
  <!-- popup register-->
  <div class="modal fade register" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="border-radius:0;border:0; display:inherit;">
      <button type="button" class="close close-btnpop" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="popdiv">
      <div class="row ml-0 mr-0 h-100" style="margin:0;">
        <div class="col-lg-5 col-md-5 col-sm-12 image-side">
          <p class="whitecolor h2">Network, Share and Learn through Events</p>
          <p class="whitecolor mb-2">Make the most out of your next event.</p>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-12 form-side-padd">
          <h3 class="backcolor bottom-margin20 top-margin0">Register</h3>
		  @include('auth.ajax-register')
        <div class="cl"></div> 
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

  <div class="modal fade login" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="border-radius:0;border:0; display:inherit;">
      <button type="button" class="close close-btnpop" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="popdiv">
      <div class="row ml-0 mr-0 h-100" style="margin:0;">
        <div class="col-lg-5 col-md-5 col-sm-12 image-side" style="padding-top:40px; padding-bottom:40px;">
          <p class="whitecolor h2">Network, Share and Learn through Events</p>
          <p class="whitecolor mb-2">Make the most out of your next event.</p>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-12 form-side-padd">
          <div class="login-content">@include('auth.ajax-login')</div>
          
        <div class="cl"></div> 
        </div>
      </div>
      </div>
    </div>
  </div>
</div>
  <!-- popup -->
</div>
<!-- /Wrap all content --> 

<!-- JS Global --> 

<!--[if lt IE 9]><script src="assets/plugins/jquery/jquery-1.11.1.min.js"></script><![endif]--> 
<!--[if gte IE 9]><!--><script src="assets/plugins/jquery/jquery-2.1.1.min.js"></script><!--<![endif]--> 
<script src="assets/plugins/modernizr.custom.js"></script> 
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/plugins/bootstrap-select/bootstrap-select.min.js"></script> 
<script src="assets/plugins/superfish/js/superfish.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/placeholdem.min.js"></script> 
<script src="assets/plugins/jquery.smoothscroll.min.js"></script> 
<script src="assets/plugins/jquery.easing.min.js"></script> 

<!-- JS Page Level --> 
<script src="assets/plugins/owlcarousel2/owl.carousel.min.js"></script> 
<script src="assets/plugins/waypoints/waypoints.min.js"></script> 
<script src="assets/plugins/countdown/jquery.plugin.min.js"></script> 
<script src="assets/plugins/countdown/jquery.countdown.min.js"></script> 
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script> 
<script src="assets/js/theme-ajax-mail.js"></script> 
<script src="assets/js/theme.js"></script> 
<script src="assets/js/custom.js"></script> 
<script type="text/javascript">
	$(document).ready(function() {
		
		$("body").on('submit', '#loginform', function(e) {
		
			$('.login-loader').attr('style', '');
			$('.login-button-submit').attr('disabled', 'disabled');
					
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			e.preventDefault();
			var data = $( this ).serialize();
			$('.login-error-msg').remove();
			$.ajax({
				url: '{{ route('login') }}',
				type: 'POST',
				data: data,
				success: function(response) {
					//location.reload();
					window.location.href = 'counter';
				},
				error: function (jqXHR) {
					$('.login-loader').attr('style', 'display:none');
					$('.login-button-submit').removeAttr('disabled');
					var response = $.parseJSON(jqXHR.responseText);
					if(response.message) {
						if(response.errors) {
							var target = $('#loginform');
							$.each(response.errors, function(field, msg) {
								$('#'+field, $('#loginform', $('.form-side-padd'))).after('<span class="text-danger login-error-msg">'+msg+'</span>');
							});
						}
					}
				}
			});
			return false;
		});
	
		$("body").on('submit', '#registerfrom', function(e) {
			var flag = true;
			var target = $('#registerfrom')
			var emailObj = $('#email', target);
			var nameObj = $('#name', target);
			var passwordObj = $('#password', target);
			$('.register-error-msg', target).remove();
			if(nameObj.val() == '') {
				nameObj.after('<strong class="alert-danger register-error-msg">The name field is required.</strong>');
				flag = false;
			}
			
			if(emailObj.val() == '') {
				emailObj.after('<strong class="alert-danger register-error-msg">The email field is required.</strong>');
				flag = false;
			} else {
				var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				if(!regex.test(emailObj.val())) {
					$('#email', target).after('<strong class="alert-danger register-error-msg">The email must be a valid email address.</strong>');
					flag = false;
				}
			}
			
			
			if(passwordObj.val() == '') {
				passwordObj.after('<strong class="alert-danger register-error-msg">The password field is required.</strong>');
				flag = false;
			} else {
				if(passwordObj.val().length < 8 ) {
					passwordObj.after('<strong class="alert-danger register-error-msg">The password must be at least 8 characters.</strong>');
					flag = false;
				} else {
					
				}
			}
			
			if(flag == false) {
				return false;
			}
			
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			e.preventDefault();
			var data = $( this ).serialize();
			$('.register-error-msg').remove();
			$('.register-loader').attr('style', '');
			$('.register-button-submit').attr('disabled', 'disabled');
			$.ajax({
				url: '{{ route('register') }}',
				type: 'POST',
				data: data,
				success: function(response) {
					 location.reload();
				},
				error: function (jqXHR) {
					$('.register-loader').attr('style', 'display:none');
					$('.register-button-submit').removeAttr('disabled');
					var response = $.parseJSON(jqXHR.responseText);
					if(response.message) {
						if(response.errors) {
							var target = $('#registerfrom');
							$.each(response.errors, function(field, msg) {
								$('#'+field, $('#registerfrom', $('.form-side-padd'))).after('<span class="text-danger register-error-msg">'+msg+'</span>');
							});
						}
					}
				}
			});
			return false;
		 });
	});
    jQuery(document).ready(function () {
        theme.init();
        theme.initMainSlider();
        theme.initCountDown();
        theme.initPartnerSlider();
        theme.initTestimonials();
        theme.initGoogleMap();
    });
    jQuery(window).load(function () {
        theme.initAnimation();
    });

    jQuery(window).load(function () { jQuery('body').scrollspy({offset: 100, target: '.navigation'}); });
    jQuery(window).load(function () { jQuery('body').scrollspy('refresh'); });
    jQuery(window).resize(function () { jQuery('body').scrollspy('refresh'); });

    jQuery(document).ready(function () { theme.onResize(); });
    jQuery(window).load(function(){ theme.onResize(); });
    jQuery(window).resize(function(){ theme.onResize(); });

    jQuery(window).load(function() {
        if (location.hash != '') {
            var hash = '#' + window.location.hash.substr(1);
            if (hash.length) {
                jQuery('html,body').delay(0).animate({
                    scrollTop: jQuery(hash).offset().top - 44 + 'px'
                }, {
                    duration: 1200,
                    easing: "easeInOutExpo"
                });
            }
        }
    });

</script>


</body>
</html>
