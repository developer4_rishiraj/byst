<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index','get_state_bycid', 'get_citiy_bycid']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$countries = DB::table('countries')->get();
        return view('welcome', compact('countries'));
    }
	
	public function get_state_bycid(Request $request)
    {
		$data = $request->all();
		$statedata = DB::table('states')->where('country_id', '=',$data['id'])->get();		
        return $statedata; 
    }
	
	public function get_citiy_bycid(Request $request)
    {
		$data = $request->all();
		$statedata = DB::table('cities')->where('state_id', '=',$data['id'])->get();		
        return $statedata; 
    }
	
	public function counter()
    {
        return view('counter');
    }
	
	public function bystexpo()
    {
        return view('bystexpo');
    }
}
